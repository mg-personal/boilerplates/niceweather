# NiceWeather

### [Check out the demo here](www.notavailableyet.com)

## About ⭐️

NiceWeather is an application designed to provide information about your place **weather**.
It is an app created with **React Native** with solely learning purposes.

## Technology 💡

## Screenshots 📷

#### Home

<!-- ![home](https://i.imgur.com/1VLMsaz.png) -->

#### Info

## License ⚠️

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
