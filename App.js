import React, { useState, useEffect } from "react";
import { SafeAreaView, Text, View, TextInput, Button } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { Ionicons, Entypo } from "@expo/vector-icons";

const Home = () => {
  const [Input, setInput] = useState();
  const [City, setCity] = useState(null);
  const [Temp, setTemp] = useState(null);

  async function getWeather(city) {
    fetch(
      `https://api.weatherbit.io/v2.0/current?city=${city}&country=AR&key=92bddf9984704995825bbbf6e2c8faeb&lang=es`
    )
      .then((res) => res.json())
      .then((data) => {
        let temp = data.data[0].app_temp;
        let city = data.data[0].city_name;
        setCity(city);
        setTemp(temp);
      });
  }

  function setWeather() {
    getWeather(Input);
    setInput("");
  }

  useEffect(() => {
    // getWeather();
  }, []);

  return (
    <View style={{ flex: 1 }}>
      <Text
        style={{
          marginTop: 15,
          marginLeft: 5,
          fontSize: 40,
          fontWeight: "700",
        }}
      >
        Search
      </Text>
      <View>
        <TextInput
          style={{
            backgroundColor: "lightgray",
            marginVertical: 15,
            marginHorizontal: 15,
            paddingVertical: 8,
            paddingHorizontal: 5,
            fontSize: 20,
            fontWeight: "500",
            borderRadius: 2,
          }}
          onChangeText={(text) => setInput(text)}
          value={Input}
        />
      </View>
      <View
        style={{
          padding: 2,
          backgroundColor: "tomato",
          marginVertical: 10,
          marginHorizontal: 10,
          borderRadius: 5,
        }}
      >
        <Button color="black" onPress={setWeather} title="Set Weather" />
      </View>
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        {City === null ? (
          <Text>No City</Text>
        ) : (
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <Text
              style={{ fontSize: 32, fontWeight: "600", marginVertical: 10 }}
            >
              {City}
            </Text>
            <Text
              style={{ fontSize: 40, fontWeight: "700", marginVertical: 10 }}
            >
              {Temp}
            </Text>
          </View>
        )}
      </View>
    </View>
  );
};

const HomeNavigator = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Home" component={Home} />
    </Stack.Navigator>
  );
};

const Info = () => {
  return (
    <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
      <Text>This is the Info Component</Text>
    </View>
  );
};

const InfoNavigator = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Info" component={Info} />
    </Stack.Navigator>
  );
};

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Tab.Navigator
        initialRouteName="HomeNavigator"
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => {
            let iconName;

            if (route.name === "HomeNavigator") {
              iconName = focused ? "ios-home" : "ios-home";
            } else if (route.name === "InfoNavigator") {
              iconName = focused
                ? "ios-information-circle"
                : "ios-information-circle-outline";
            }
            return <Ionicons name={iconName} size={size} color={color} />;
          },
        })}
        tabBarOptions={{
          activeTintColor: "tomato",
          inactiveTintColor: "gray",
        }}
      >
        <Tab.Screen
          name="HomeNavigator"
          component={HomeNavigator}
          options={{ title: "Home" }}
        />
        <Tab.Screen
          name="InfoNavigator"
          component={InfoNavigator}
          options={{ title: "Info" }}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );
}
